package com.example.hangmannapp

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.example.hangmannapp.databinding.ActivityGameScreenBinding
import kotlin.properties.Delegates

class Game_Screen : AppCompatActivity(), View.OnClickListener{
    lateinit var binding: ActivityGameScreenBinding
    lateinit var palabrasecreta: String
    lateinit var difficulty: String
    var correctletter: Int = 0
    lateinit var guiones: MutableList<String>
    var attempts=0
    lateinit var imagenes: List<Int>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGameScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val bundle: Bundle? = intent.extras
        difficulty = bundle?.getString("dificult_value").toString()

        imagenes= listOf(R.drawable.hangman_1, R.drawable.hangman_2, R.drawable.hangman_3, R.drawable.hangman_4, R.drawable.hangman_5, R.drawable.hangman_6)

       when(difficulty){
            "Difícil" -> palabrasecreta= mutableListOf<String>("EXHAUSTIVO", "INCENTIVAR", "METICULOSO", "PREOCUPADO", "PRIVILEGIO", "INNOVACION", "TOLERANCIA", "MAJESTUOSO", "HABITACION", "SUPERVISAR").random()
            "Normal" -> palabrasecreta= mutableListOf<String>("INTERES", "SUBLIME", "MONTAÑA", "HONESTO", "ESCASEZ", "ENFOQUE", "MALVADO", "CRITICO", "POBREZA", "INMERSO").random()
            "Fácil" -> palabrasecreta= mutableListOf<String> ("CAMPO", "TECHO", "TONTO", "RESTA", "CAJON", "PISTA", "BULOS", "CAIDA", "FUSIL", "EDITA").random()
        }
        attempts = 0
        guiones = MutableList(palabrasecreta.length){ "_" }
        binding.palabra.text = guiones.toString().replace("[", "").replace(",", "").replace("]", "")

        binding.letterA.setOnClickListener(this)
        binding.letterB.setOnClickListener(this)
        binding.letterC.setOnClickListener(this)
        binding.letterD.setOnClickListener(this)
        binding.letterE.setOnClickListener(this)
        binding.letterF.setOnClickListener(this)
        binding.letterG.setOnClickListener(this)
        binding.letterH.setOnClickListener(this)
        binding.letterI.setOnClickListener(this)
        binding.letterJ.setOnClickListener(this)
        binding.letterK.setOnClickListener(this)
        binding.letterL.setOnClickListener(this)
        binding.letterM.setOnClickListener(this)
        binding.letterN.setOnClickListener(this)
        binding.letterEnye.setOnClickListener(this)
        binding.letterO.setOnClickListener(this)
        binding.letterP.setOnClickListener(this)
        binding.letterQ.setOnClickListener(this)
        binding.letterR.setOnClickListener(this)
        binding.letterS.setOnClickListener(this)
        binding.letterT.setOnClickListener(this)
        binding.letterU.setOnClickListener(this)
        binding.letterV.setOnClickListener(this)
        binding.letterW.setOnClickListener(this)
        binding.letterX.setOnClickListener(this)
        binding.letterY.setOnClickListener(this)
        binding.letterZ.setOnClickListener(this)
    }



    @SuppressLint("SetTextI18n")
    override fun onClick(p0: View?) {
        val button = p0 as Button


        var palabraendivinada= ""
        val letra = button.text.toString()


        if (letra in palabrasecreta){
            for (i in palabrasecreta.indices) {
                if (letra == palabrasecreta[i].toString()) {
                    guiones[i]= letra
                    correctletter++
                }
            }
        }
        else {
            binding.intentos.text = "Intentos: ${attempts+1}/6"
                binding.imageAhorcado.setImageResource(imagenes[attempts])
                attempts++
        }
        button.visibility = View.INVISIBLE
        for (letraG in guiones) palabraendivinada += letraG
        binding.palabra.text = guiones.toString().replace("[", "").replace(",", "").replace("]", "")

        if (attempts == 6 || correctletter==palabrasecreta.length){
            val intent = Intent(this, ResultScreen::class.java)
            intent.putExtra("palabra", palabrasecreta)
            intent.putExtra("intentos", attempts)
            startActivity(intent)
        }
    }
}
