package com.example.hangmannapp

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.hangmannapp.databinding.ActivityMenuScreenBinding


class MenuScreen : AppCompatActivity() {
    lateinit var binding: ActivityMenuScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonPlay.setOnClickListener {
            val intent = Intent(this, Game_Screen::class.java)
            intent.putExtra("dificult_value", binding.spinnerDifficult.selectedItem.toString())
            startActivity(intent)
        }
        binding.buttonHelp.setOnClickListener {
            val alertDialog= AlertDialog.Builder(this).create()
            alertDialog.setTitle("INSTRUCCIONES")
            alertDialog.setMessage("El jugador que intenta adivinar comenzará a decir sus letras.\n" +
                    "\n" +
                    "En caso de acertar una letra, se colocará en el espacio correspondiente.\n" +
                    "\n" +
                    "En el caso de no acertar, comenzarás a dibujar el ahorcado, empezando por la cabeza, el tronco, las piernas y así sucesivamente.\n" +
                    "\n" +
                    "Si el jugador no adivina la palabra será ahorcado y finalizará automáticamente el juego.")
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
            alertDialog.show()
        }




    }
}