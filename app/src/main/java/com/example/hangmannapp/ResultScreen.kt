package com.example.hangmannapp

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.hangmannapp.databinding.ActivityResultScreenBinding

class ResultScreen : AppCompatActivity() {
    lateinit var binding: ActivityResultScreenBinding

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResultScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle: Bundle? = intent.extras
        val attempts = bundle?.getInt("intentos")
        val palabra = bundle?.getString("palabra")


        if (attempts==6){
            binding.felicitacion.text = "LO SIENTO, HAS PERDIDO \uD83D\uDE2B"
            binding.adivinado.text = "HAS UTILIZADO LOS 6 INTETNOS PARA ADIVINAR LA PALABRA $palabra"
        }

        if (attempts != 6){
            binding.felicitacion.text = "FELICIDADES \uD83C\uDF89"
            binding.adivinado.text = " HAS ADIVINADO LA PALABRA $palabra EN $attempts INTENTOS"
        }


        binding.playagainButton.setOnClickListener {
            val intent = Intent(this, Game_Screen::class.java)
            startActivity(intent)
        }
        binding.returnmenuButton.setOnClickListener {
            val intent = Intent(this, MenuScreen::class.java)
            startActivity(intent)
        }

    }
}